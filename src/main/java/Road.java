public class Road {
    private City cityOrigin;
    private City cityDestination;
    private double distance;

    public Road(City cityOrigin, City cityDestination, double distance) {
        this.cityOrigin = cityOrigin;
        this.cityDestination = cityDestination;
        this.distance = distance;
    }

    public City getCityDestination() {
        return cityDestination;
    }

    public City getCityOrigin() {
        return cityOrigin;
    }

    public double getDistance() {
        return distance;
    }

    public String toJson() {
        StringBuilder jsonBuilder = new StringBuilder();
        jsonBuilder.append("{");
        jsonBuilder.append("\"" + "origin\": " + "\"" + getCityOrigin().getId() + "\"" + ",");
        jsonBuilder.append("\"" + "destination\": " + "\"" + getCityDestination().getId() + "\"" + ",");
        jsonBuilder.append("\"" + "distance\": " + getDistance());
        jsonBuilder.append("}");
        return jsonBuilder.toString();
    }
}
