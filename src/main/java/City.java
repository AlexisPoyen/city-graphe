import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.lang.Math;
import java.util.LinkedList;

public class City implements Comparable<City> {
    private String id;
    private String nom;
    private int population;
    private double longitude;
    private double latitude;
    private LinkedList<Road> successor;

    private static final double rayonTerre = 6378000;
    private static LinkedList<City> citiesList;

    public City(String id, String nom, int population, double longitude, double latitude) {
        this.id = id;
        this.nom = nom;
        this.population = population;
        this.longitude = longitude;
        this.latitude = latitude;
        this.successor = new LinkedList<>();
    }

    public String getId() {
        return id;
    }

    public int getPopulation() {
        return population;
    }

    public void findSuccessor(int distanceBetweenCities) {
        for (City city : citiesList) {
            if(!city.id.equals(this.id)) {
                double distance = this.distanceTo(city);
                if (distance < distanceBetweenCities)
                    successor.add(new Road(this, city, distance));
            }
        }
    }

    public double getLongitude() {
        return longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public LinkedList<Road> getSuccessor() {
        return successor;
    }

    public double distanceTo(City city) {
        // https://www.movable-type.co.uk/scripts/latlong.html
        //var φ1 = lat1.toRadians();
        //var φ2 = lat2.toRadians();
        //var Δφ = (lat2-lat1).toRadians();
        //var Δλ = (lon2-lon1).toRadians();
        //
        //var a = Math.sin(Δφ/2) * Math.sin(Δφ/2) +
        //        Math.cos(φ1) * Math.cos(φ2) *
        //        Math.sin(Δλ/2) * Math.sin(Δλ/2);
        //var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        //
        //var d = R * c;
        double a = Math.sin(Math.toRadians(city.latitude-this.latitude)/2) * Math.sin(Math.toRadians(city.latitude-this.latitude)/2)
                + Math.cos(Math.toRadians(this.latitude)) * Math.cos(Math.toRadians(city.latitude)) * Math.sin(Math.toRadians(city.longitude-this.longitude)/2)
                * Math.sin(Math.toRadians(city.longitude-this.longitude)/2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        return rayonTerre * c /1000;


    }

    public String getNom() {
        return nom;
    }

    public static LinkedList<City> createCitiesFromCSV() {
        citiesList = new LinkedList<>();
        String csvFile = "CommunesFranceCut.csv";
        String line = "";
        String cvsSplitBy = ";";
        String[] cityCSV;

        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {
            line = br.readLine(); //drop first line with headers
            while ((line = br.readLine()) != null) {
                cityCSV = line.split(cvsSplitBy);
                cityCSV[3] = cityCSV[3].replace(',', '.');
                cityCSV[4] = cityCSV[4].replace(',', '.');
                citiesList.add(new City(cityCSV[0],
                        cityCSV[1],
                        Integer.parseInt(cityCSV[2]),
                        Double.parseDouble(cityCSV[3]),
                        Double.parseDouble(cityCSV[4])));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return citiesList;
    }

    public String toJson() {
        StringBuilder jsonBuilder = new StringBuilder();
        jsonBuilder.append("{");
        jsonBuilder.append("\"" + "id\": " + "\"" + this.getId() + "\"" + ",");
        jsonBuilder.append("\"" + "nom\": " + "\"" + this.getNom() + "\"" + ",");
        jsonBuilder.append("\"" + "population\": " + this.getPopulation() + ",");
        jsonBuilder.append("\"" + "longitude\": " + this.getLongitude() + ",");
        jsonBuilder.append("\"" + "latitude\": " + this.getLatitude());
        jsonBuilder.append("}");
        return jsonBuilder.toString();
    }

    @Override
    public int compareTo(City o) {
        return this.getId().compareToIgnoreCase(o.getId());
    }
}
