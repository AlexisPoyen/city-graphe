import com.sun.javafx.geom.Edge;

import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;

public class DijkstraFibonacciHeap {

    public static LinkedHashMap<City, City> computePathsFibonacciHeap(LinkedList<City> Q, City source) {
        {
            FibonacciHeap<City> myHeap = new FibonacciHeap<>();
            LinkedHashMap<City, Double> distList = new LinkedHashMap<>();
            LinkedHashMap<City, City> predecessorList = new LinkedHashMap<>();
            for (City city : Q) {
                distList.put(city, Double.MAX_VALUE);
            }
            distList.replace(source, (double) 0);
            myHeap.enqueue(source, distList.get(source));

            while (!myHeap.isEmpty()) {
                City u = myHeap.dequeueMin().getValue();
                if (u == null) {
                    System.out.println("Certains points ne peuvnet pas être atteints");
                    return predecessorList;
                } else {
                    System.out.println("Selected city : " + u.getId());
                }

                // Visit each edge exiting u
                for (Road e : u.getSuccessor()) {
                    City v = e.getCityDestination();
                    double weight = e.getDistance();
                    double distanceThroughU = distList.get(u) + weight;
                    if (distList.get(v) != null && distanceThroughU < distList.get(v)) {
                        distList.replace(v, distanceThroughU);
                        myHeap.enqueue(v, distList.get(v));
                        predecessorList.put(v, u);
                    }
                }
            }
            return predecessorList;
        }
    }

    public static LinkedList<City> findSmallestWay(LinkedList<City> graph, City startPoint, City endPoint) {
        LinkedHashMap<City, City> predecessorList = computePathsFibonacciHeap(graph, startPoint);
        LinkedList<City> way = new LinkedList<>();
        City s = endPoint;
        while (s != startPoint) {
            way.add(s);
            s = predecessorList.get(s);
            if (s == null) {
                return null;
            }
        }
        way.add(s);
        return way;
    }

}
