import com.fasterxml.jackson.core.FormatSchema;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;

import static spark.Spark.*;

public class Main {
    public static void main(String[] args) {
//         LinkedList<City> listCities = createGraph();
        LinkedList<City> listCities = createCityGraph();
//        System.out.println(listCities.get(7).getId() + listCities.get(0).getId());
//        LinkedList<City> smallestWay = DijkstraFibonacciHeap.findSmallestWay(listCities, listCities.get(0), listCities.get(7));
//        smallestWay.forEach(city -> System.out.println(city.getId()));
        runApi(listCities);
    }

    private static void runApi(LinkedList<City> listCities) {
        options("/*",
                (request, response) -> {

                    String accessControlRequestHeaders = request
                            .headers("Access-Control-Request-Headers");
                    if (accessControlRequestHeaders != null) {
                        response.header("Access-Control-Allow-Headers",
                                accessControlRequestHeaders);
                    }

                    String accessControlRequestMethod = request
                            .headers("Access-Control-Request-Method");
                    if (accessControlRequestMethod != null) {
                        response.header("Access-Control-Allow-Methods",
                                accessControlRequestMethod);
                    }

                    return "OK";
                });

        before((request, response) -> response.header("Access-Control-Allow-Origin", "*"));


        get("/graph", (req, res) -> {
            System.out.println("Creating json");
            String jsonCity = listCityToJson(listCities);
            System.out.println("json created, sending...");
            return jsonCity;
        });
        get("/graph/:algo//", (req, res) -> {
            res.status(500);
            return "{\"message\":\"Invalid request\"}";
        });
        get("/graph/:algo/:from/:to", (req, res) -> {
            res.type("application/json");
            String from = req.params(":from");
            String to = req.params(":to");
            String algo = req.params(":algo");
            System.out.println("Searching for city from");
            int j = 0;
            while (j < listCities.size() && !listCities.get(j).getId().equals(from)) {
                j++;
            }
            if (j == listCities.size()) {
                res.status(404);
                return "{\"message\":\"City not found\"}";
            }
            System.out.println("Searching for city 2");
            int k = 0;
            while (k < listCities.size() && !listCities.get(k).getId().equals(to)) {
                k++;
            }
            if (k == listCities.size()) {
                res.status(404);
                return "{\"message\":\"City not found\"}";
            }
            System.out.println("finding smallest way...");
            LinkedList<City> smallestWay;
            long startTime = System.nanoTime();
            if (algo.equals("dijkstra")) smallestWay = Dijkstra.findSmallestWay(listCities, listCities.get(j), listCities.get(k));
            else if (algo.equals("astar")) smallestWay = AEtoile.smallestWay(listCities, listCities.get(j), listCities.get(k));
            else if (algo.equals("dijkstraFib")) smallestWay = DijkstraFibonacciHeap.findSmallestWay(listCities, listCities.get(j), listCities.get(k));
            else {
                res.status(404);
                return "{\"message\":\"Algorithm not found\"}";
            }
            long endTime = System.nanoTime();
            long duration = (endTime - startTime);
            if (smallestWay == null) {
                res.status(404);
                return "{\"message\":\"No way possible\"}";
            }
            System.out.println("smallest way found");
            res.status(200);
            System.out.println("Creating json");
            StringBuilder jsonCity = new StringBuilder();
            jsonCity.append("{");
            jsonCity.append("\"duration\":" + duration + ",");
            jsonCity.append("\"list\":" + listCityToJson(smallestWay));
            jsonCity.append("}");
            System.out.println("json created, sending...");
            System.out.println(jsonCity.toString());
            return jsonCity.toString();
        });
    }

    private static LinkedList<City> createCityGraph() {
        int distanceBetweenCities = 25;
        System.out.println("Creating city list...");
        LinkedList<City> listCities = City.createCitiesFromCSV();
        System.out.println("City list created !");
        System.out.println("Finding successors...");
        int i = 1;
        for(City city : listCities){
            System.out.println(+ i + "/" + listCities.size() + " finding successor for " + city.getId());
            i++;
            city.findSuccessor(distanceBetweenCities);
        }
        System.out.println("Successors found !");
        return listCities;
    }

    private static String listCityToJson(LinkedList<City> listCities) {
        StringBuilder responseSB = new StringBuilder();
        responseSB.append("[");
        for (int j = 0; j < listCities.size() - 1; j++) {
            City city = listCities.get(j);
            responseSB.append("{"
                    + "\"id\": " + "\"" + city.getId() + "\" ,"
                    + "\"nom\": " + "\"" + city.getNom() + "\" ,"
                    + "\"population\": " + city.getPopulation() + " ,"
                    + "\"longitude\": " + city.getLongitude() + " ,"
                    + "\"latitude\": " + city.getLatitude()
                    + "},");
        }
        City city = listCities.get(listCities.size() - 1);
        responseSB.append("{"
                + "\"id\": " + "\"" + city.getId() + "\" ,"
                + "\"nom\": " + "\"" + city.getNom() + "\" ,"
                + "\"population\": " + city.getPopulation() + " ,"
                + "\"longitude\": " + city.getLongitude() + " ,"
                + "\"latitude\": " + city.getLatitude()
                + "}");
        responseSB.append("]");
        return responseSB.toString();
    }


    private static LinkedList<City> createGraph() {
        City a = new City("A", "A", 0, 0, 0);
        City b = new City("B", "B", 0, 0, 0);
        City c = new City("C", "C", 0, 0, 0);
        City d = new City("D", "D", 0, 0, 0);
        City e = new City("E", "E", 0, 0, 0);
        City f = new City("F", "F", 0, 0, 0);
        City g = new City("G", "G", 0, 0, 0);
        City h = new City("H", "H", 0, 0, 0);
        a.getSuccessor().push(new Road(a, e, 5));
        a.getSuccessor().push(new Road(a, b, 4));

        b.getSuccessor().push(new Road(b, c, 2));

        c.getSuccessor().push(new Road(c, h, 11));

        d.getSuccessor().push(new Road(d, a, 15));
        d.getSuccessor().push(new Road(d, h, 4));

        e.getSuccessor().push(new Road(e, f, 5));
        e.getSuccessor().push(new Road(e, b, 9));
        e.getSuccessor().push(new Road(e, d, 20));

        f.getSuccessor().push(new Road(f, b, 7));

        g.getSuccessor().push(new Road(g, f, 3));
        g.getSuccessor().push(new Road(g, b, 8));
        g.getSuccessor().push(new Road(g, c, 6));

        h.getSuccessor().push(new Road(h, f, 9));

        LinkedList<City> listCities = new LinkedList<>();
        listCities.push(a);
        listCities.push(b);
        listCities.push(c);
        listCities.push(d);
        listCities.push(e);
        listCities.push(f);
        listCities.push(g);
        listCities.push(h);

        return listCities;


    }
}
