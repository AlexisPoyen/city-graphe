import java.sql.Time;
import java.util.*;

public class Dijkstra {

    private static LinkedHashMap<City, City> processDijkstra(LinkedList<City> graph, City startPoint) {
        // Initialisation

        LinkedHashSet<City> Q = new LinkedHashSet<>(graph);
        LinkedHashMap<City, Double> distHashMap = new LinkedHashMap<>();
        LinkedHashMap<City, City> predecessorList = new LinkedHashMap<>();
        for (City city : Q) {
            distHashMap.put(city, Double.MAX_VALUE);
        }
        distHashMap.replace(startPoint, (double) 0);

        while (!Q.isEmpty()) {
            City selectedCity = findMin(Q, distHashMap);
            Q.remove(selectedCity);
            if (selectedCity == null) {
                System.out.println("Certains points ne peuvnet pas être atteints");
                return predecessorList;
            } else {
                System.out.println("Selected city : " + selectedCity.getId());
            }
            if (selectedCity != null && selectedCity.getSuccessor() != null) {
                for (Road successor : selectedCity.getSuccessor()) {
                    City successorCity = successor.getCityDestination();
                    double tmp = distHashMap.get(selectedCity) + successor.getDistance();
                    if (distHashMap.get(successorCity) != null && tmp < distHashMap.get(successorCity)) {
                        distHashMap.replace(successorCity, tmp);
                        predecessorList.put(successorCity, selectedCity);
                    }
                }
            }
        }

        return predecessorList;
    }

    public static LinkedList<City> findSmallestWay(LinkedList<City> graph, City startPoint, City endPoint) {
        LinkedHashMap<City, City> predecessorList = processDijkstra(graph, startPoint);
        LinkedList<City> way = new LinkedList<>();
        City s = endPoint;
        while (s != startPoint) {
            way.add(s);
            s = predecessorList.get(s);
            if (s == null) {
                return null;
            }
        }
        way.add(s);
        return way;
    }

    private static double findWeight(City startPoint, City currentPoint) {
        for (Road successor : startPoint.getSuccessor()) {
            if (successor.getCityDestination().getId().equals(currentPoint.getId())) {
                return successor.getDistance();
            }
        }
        return Double.MAX_VALUE;
    }

    private static City findMin(LinkedHashSet<City> Q, LinkedHashMap<City, Double> distHashMap) {
        double min = Double.MAX_VALUE;
        City top = null;
        for (City s : Q) {
            Double dist = distHashMap.get(s);
            if (dist != null && dist < min) {
                min = dist;
                top = s;
            }
        }
        return top;
     }
}
